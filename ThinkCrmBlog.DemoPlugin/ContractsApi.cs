﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class ContractsApi : IPlugin
    {

        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            if (context.Depth == 2)
            {
                return;
            }
            try
            {
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
            tracer.Trace("Into GovWin plugin to call api contract");
                    //var targetEntity = (Entity)context.InputParameters["Target"];
                    //if (targetEntity.LogicalName != "govwin_govcontracts")
                    //    return;
                    ////Entity contracts = service.Retrieve("govwin_govcontracts", targetEntity.Id, new ColumnSet(true));
                    //Entity contracts = (Entity)context.InputParameters["Target"];
                    var opportunity = (Entity)context.InputParameters["Target"];
                    var targetEntity = opportunity.GetAttributeValue<EntityReference>("govwin_govwincontracts");
                    Entity contracts = service.Retrieve("govwin_govcontracts", targetEntity.Id, new ColumnSet(new string[] { "govwin_token", "govwin_href" }));
                    if (contracts.Attributes.Contains("govwin_token") && contracts.Attributes.Contains("govwin_href"))
                    {
                        tracer.Trace("Into  contract token " + contracts.Attributes["govwin_token"].ToString());
                        tracer.Trace("Into  contract link " + contracts.Attributes["govwin_href"].ToString());
                        var ovrelateddoclist = getcontracts(contracts.Attributes["govwin_href"].ToString(), contracts.Attributes["govwin_token"].ToString(), tracer);
                        EntityReferenceCollection ovrelateddocEntity = new EntityReferenceCollection();
                        Guid contractguid = new Guid();
                        int i = 0;
                        tracer.Trace("Into  contract after response " + ovrelateddoclist.Count());
                        foreach (var c in ovrelateddoclist)
                        {
                            i++;
                            try
                            {

                                Entity contptype = new Entity("govwin_subgovcontracts");
                                if (c.awardDate != "" && c.awardDate != null)
                                {
                                    contptype["govwin_awarddate"] = Convert.ToDateTime(c.awardDate);
                                }
                                if (c.company[0].name != "" && c.company[0].name != null)
                                {
                                    contptype["govwin_company"] = c.company[0].name;
                                }
                                if (c.contractNumber != "" && c.contractNumber != null)
                                {
                                    contptype["govwin_contactname"] = c.contractNumber;
                                }
                                if (c.contractNumber != "" && c.contractNumber != null)
                                {
                                    contptype["govwin_contractnumber"] = (c.contractNumber);
                                }
                                if ((c.estimatedValue) != 0)
                                {
                                    contptype["govwin_estimatedvalue"] = (c.estimatedValue);
                                }
                                if (c.expirationDate != "" && c.expirationDate != null)
                                {
                                    contptype["govwin_expirationdate"] = Convert.ToDateTime(c.expirationDate);
                                }
                                if (c.id != 0)
                                {
                                    contptype["govwin_id"] = (c.id);
                                    contptype["govwin_id2"] = Convert.ToString(c.id);
                                }
                                if (c.incumbent != "" && c.incumbent != null)
                                {
                                    contptype["govwin_incumbent"] = Convert.ToString(c.incumbent);
                                }
                                if (c.taskOrderNumber != "" && c.taskOrderNumber != null)
                                {
                                    contptype["govwin_taskordernumber"] = (c.taskOrderNumber);
                                }
                                if (c.title != "" && c.title != null)
                                {
                                    contptype["govwin_title"] = (c.title);
                                }
                                contptype["govwin_name"] = (i.ToString());
                                EntityReference erf = new EntityReference("govwin_govcontracts", targetEntity.Id);
                                contptype["govwin_contractslist"] = erf;

                                contractguid = service.Create(contptype);
                                tracer.Trace("Into  contract after response added");
                                EntityReference CnTr = new EntityReference();
                                CnTr.LogicalName = "govwin_subgovcontracts";
                                CnTr.Id = contractguid;
                                ovrelateddocEntity.Add(CnTr);
                            }
                            catch (Exception ex)
                            {
                                tracer.Trace("Into  contract after response added erreor " + ex.Message);
                            }
                        }
                        //if (ovrelateddocEntity.Count > 0)
                        //{
                        //    tracer.Trace("count of related count "+ ovrelateddocEntity.Count);
                        //    Relationship ovrelateddoc = new Relationship("govwin_govwin_govcontracts_govwin_subgovcontracts");
                        //    service.Associate("govwin_govcontracts", contracts.Id, ovrelateddoc, ovrelateddocEntity);
                        //    tracer.Trace("Into  contract after response association " );
                        //    ovrelateddocEntity = null;
                        //}
                    }

                }
                //TODO: Do stuff
            }
            catch (Exception e)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }
        }

        public List<Contract> getcontracts(string url, string token, ITracingService trace)
        {
            List<Contract> contractslist = new List<Contract>();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers["Authorization"] = token;
                request.Method = "GET";
                request.KeepAlive = false;
                try
                {
                    WebResponse response = request.GetResponse();
                    trace.Trace("Into contract got response ");
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        var json = JsonConvert.DeserializeObject<RootObjectContracts>(result);
                        contractslist = json.contracts;
                        trace.Trace("Into contract got serialize ");
                    }
                    response.Dispose();
                }
                catch (Exception exe)
                {
                    trace.Trace("Into contract erroer inner " + exe.Message);
                }

            }
            catch (Exception exe)
            {
                trace.Trace("Into contract erroer outer " + exe.Message);
            }

            return contractslist;
        }



    }

    public class Company
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    public class Contract
    {
        public Links links { get; set; }
        public string awardDate { get; set; }//incumbent
        public int estimatedValue { get; set; }
        public string expirationDate { get; set; }
        public string incumbent { get; set; }
        public string contractNumber { get; set; }
        public string taskOrderNumber { get; set; }
        public string title { get; set; }
        public List<Company> company { get; set; }
        public int id { get; set; }
    }
    public class RootObjectContracts
    {
        public Links links { get; set; }
        public Meta meta { get; set; }
        public List<Contract> contracts { get; set; }
    }
    public class Meta
    {
        public Paging paging { get; set; }
    }
    public class Paging
    {
        public int max { get; set; }
        public int offset { get; set; }
        public string order { get; set; }
        public string sort { get; set; }
        public int totalCount { get; set; }
    }
}
