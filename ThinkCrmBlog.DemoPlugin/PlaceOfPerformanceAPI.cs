﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class PlaceOfPerformanceAPI : IPlugin
    {

        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            if (context.Depth == 2)
            {
                return;
            }
            try
            {
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    tracer.Trace("Into GovWin plugin to call api POPApi");
                    //var targetEntity = (Entity)context.InputParameters["Target"];
                    //if (targetEntity.LogicalName != "govwin_govplaceofperformance")
                    //    return;
                    ////Entity POPerform = service.Retrieve("govwin_govplaceofperformance", targetEntity.Id, new ColumnSet(true));
                    //Entity POPerform = (Entity)context.InputParameters["Target"]; // App Source
                    var opportunity = (Entity)context.InputParameters["Target"];
                    var targetEntity = opportunity.GetAttributeValue<EntityReference>("govwin_govwinplaceofperformances");
                    Entity POPerform = service.Retrieve("govwin_govplaceofperformance", targetEntity.Id, new ColumnSet(new string[] { "govwin_token", "govwin_href" }));

                    if (POPerform.Attributes.Contains("govwin_token") && POPerform.Attributes.Contains("govwin_href"))
                    {
                        var ovrelateddoclist = getPOPerform(POPerform.Attributes["govwin_href"].ToString(), POPerform.Attributes["govwin_token"].ToString(), tracer);
                        EntityReferenceCollection ovrelateddocEntity = new EntityReferenceCollection();
                        Guid POPGuid = new Guid();
                        int i = 0;
                        foreach (var c in ovrelateddoclist)
                        {
                            i++;
                            try
                            {
                                Entity POP = new Entity("govwin_subgovplaceofperformance");
                                if (c.country != "" && c.country != null)
                                {
                                    POP["govwin_country"] = (c.country);
                                    POP["govwin_name"] = (c.country);
                                }
                                if (c.location != "" && c.location != null)
                                {
                                    POP["govwin_location"] = (c.location);

                                }
                                if (c.state != "" && c.state != null)
                                {
                                    POP["govwin_state"] = (c.state);
                                }
                                if (c.isPrimary)
                                {
                                    POP["govwin_isprimary"] = true;
                                }
                                else
                                {
                                    POP["govwin_isprimary"] = false;
                                }

                                EntityReference erf = new EntityReference("govwin_govplaceofperformance", targetEntity.Id);
                                POP["govwin_govwinplaceofperformance"] = erf;

                                POPGuid = service.Create(POP);

                            }
                            catch (Exception)
                            {
                            }
                        }

                    }

                }
            }
            catch (Exception)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }
        }

        public List<PlacesOfPerformances> getPOPerform(string url, string token, ITracingService trace)
        {
            List<PlacesOfPerformances> contractslist = new List<PlacesOfPerformances>();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers["Authorization"] = token;
                request.Method = "GET";
                request.KeepAlive = false;
                try
                {
                    WebResponse response = request.GetResponse();
                    trace.Trace("Into POP got response ");
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        trace.Trace(result = result.ToString());
                        var json = JsonConvert.DeserializeObject<RootObjectPOP>(result);
                        contractslist = json.placesOfPerformance;
                    }
                    response.Dispose();
                }
                catch (Exception)
                {
                    trace.Trace("Into POP erroer inner ");
                }

            }
            catch (Exception)
            {
                trace.Trace("Into POP erroer outer ");
            }

            return contractslist;
        }

    }



    public class PlacesOfPerformances
    {
        public string country { get; set; }
        public string location { get; set; }
        public string state { get; set; }
        public bool isPrimary { get; set; }
    }
    public class RootObjectPOP
    {
        public Links links { get; set; }
        public Meta meta { get; set; }
        public List<PlacesOfPerformances> placesOfPerformance { get; set; }
    }
}
