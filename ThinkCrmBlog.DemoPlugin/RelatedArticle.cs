﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class RelatedArticle : IPlugin
    {

        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            if (context.Depth == 2)
            {
                return;
            }

            try
            {
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    tracer.Trace("Into GovWin plugin to call api Related Articles");

                    //var targetEntity = (Entity)context.InputParameters["Target"];
                    //if (targetEntity.LogicalName != "govwin_govrelatedarticles")
                    //{
                    //    return;
                    //}
                    ////Entity RelatedArticles = service.Retrieve("govwin_govrelatedarticles", targetEntity.Id, new ColumnSet(true));
                    //Entity RelatedArticles = (Entity)context.InputParameters["Target"]; // App Source
                    var opportunity = (Entity)context.InputParameters["Target"];
                    var targetEntity = opportunity.GetAttributeValue<EntityReference>("govwin_govwinrelatedarticles");
                    Entity RelatedArticles = service.Retrieve("govwin_govrelatedarticles", targetEntity.Id, new ColumnSet(new string[] { "govwin_token", "govwin_href" }));
                    if (RelatedArticles.Attributes.Contains("govwin_token") && RelatedArticles.Attributes.Contains("govwin_href"))
                    {
                        //tracer.Trace("Into  Related Articles token " + RelatedArticles.Attributes["govwin_token"].ToString());
                        //tracer.Trace("Into  Related Articles link " + RelatedArticles.Attributes["govwin_href"].ToString());
                        var ovrelateddoclist = getrelatedArticles(RelatedArticles.Attributes["govwin_href"].ToString(), RelatedArticles.Attributes["govwin_token"].ToString(), tracer);
                        EntityReferenceCollection ovrelateddocEntity = new EntityReferenceCollection();
                        Guid RAguid = new Guid();
                        int i = 0;
                        foreach (var c in ovrelateddoclist)
                        {
                            i++;
                            try
                            {



                                Entity RAE = new Entity("govwin_subgovrelatedarticles");
                                if (c.documentLink != "" && c.documentLink != null)
                                {
                                    RAE["govwin_documentlink"] = (c.documentLink);
                                }
                                if (c.id != "" && c.id != null)
                                {
                                    RAE["govwin_id"] = c.id;
                                }
                                if (c.publicationDate != "" && c.publicationDate != null)
                                {
                                    RAE["govwin_publicationdate"] = (c.publicationDate);
                                }
                                if (c.source != "" && c.source != null)
                                {
                                    RAE["govwin_source"] = (c.source);
                                }
                                if (c.title != null && c.title != "")
                                {
                                    RAE["govwin_title"] = (c.title);
                                }
                                RAE["govwin_name"] = (i.ToString());
                                EntityReference erf = new EntityReference("govwin_govrelatedarticles", targetEntity.Id);
                                RAE["govwin_relatedarticallistid"] = erf;

                                RAguid = service.Create(RAE);
                                tracer.Trace("Into  Related Articles after response added");
                                EntityReference CnTr = new EntityReference();
                                CnTr.LogicalName = "govwin_subgovrelatedarticles";
                                CnTr.Id = RAguid;
                                ovrelateddocEntity.Add(CnTr);
                            }
                            catch (Exception)
                            {
                                tracer.Trace("Into  Related Articles after response added erreor ");
                            }
                        }

                    }

                }
                //TODO: Do stuff
            }
            catch (Exception)
            {
                tracer.Trace("Into  Related Articles after response added erreor ");
                //throw new InvalidPluginExecutionException(e.Message);
            }
        }


        public List<DocumentLinksRA> getrelatedArticles(string url, string token, ITracingService trace)
        {

            List<DocumentLinksRA> DocumentLinkslist = new List<DocumentLinksRA>();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers["Authorization"] = token;
                request.Method = "GET";
                request.KeepAlive = false;
                try
                {
                    WebResponse response = request.GetResponse();

                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {

                        var result = streamReader.ReadToEnd();
                        var k = result;
                        var json = JsonConvert.DeserializeObject<RootObjectRelatedArticles>(result);

                        DocumentLinkslist = json.documentLinks;
                    }
                    response.Dispose();
                }
                catch (Exception)
                { }

            }
            catch (Exception)
            {

            }

            return DocumentLinkslist;
        }


    }

    //related Articles Model
    #region

    public class DocumentLinksRA
    {
        public string documentLink { get; set; }
        public string id { get; set; }
        public string publicationDate { get; set; }
        public string title { get; set; }
        public string source { get; set; }

    }

    public class RootObjectRelatedArticles
    {
        public Links links { get; set; }
        public Meta meta { get; set; }
        public List<DocumentLinksRA> documentLinks { get; set; }
    }
    #endregion
}
