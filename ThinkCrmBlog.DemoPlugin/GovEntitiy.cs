﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class GovEntitiy : IPlugin
    {

        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            //tracer.Trace("Into GovWin plugin to call api GovEntities context.Depth:- " + (context.Depth));
            if (context.Depth == 2)
            {
                return;
            }
            try
            {
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    tracer.Trace("Into GovWin plugin to call api GovEntities");
                    //var targetEntity = (Entity)context.InputParameters["Target"];
                    //if (targetEntity.LogicalName != "govwin_govgov_entites")
                    //    return;
                    ////Entity goventity = service.Retrieve("govwin_govgov_entites", targetEntity.Id, new ColumnSet(true));
                    //Entity goventity = (Entity)context.InputParameters["Target"];
                    var opportunity = (Entity)context.InputParameters["Target"];
                    var targetEntity = opportunity.GetAttributeValue<EntityReference>("govwin_govwinentities");
                    Entity goventity = service.Retrieve("govwin_govgov_entites", targetEntity.Id, new ColumnSet(new string[] { "govwin_token", "govwin_href" }));
                    if (goventity.Attributes.Contains("govwin_token") && goventity.Attributes.Contains("govwin_href"))
                    {
                        tracer.Trace("Into  GovEntities token " + goventity.Attributes["govwin_token"].ToString());
                        tracer.Trace("Into  GovEntities link " + goventity.Attributes["govwin_href"].ToString());
                        var ovrelateddoclist = getgoventities(goventity.Attributes["govwin_href"].ToString(), goventity.Attributes["govwin_token"].ToString(), tracer);
                        EntityReferenceCollection ovrelateddocEntity = new EntityReferenceCollection();
                        Guid GovEntityguid = new Guid();
                        int i = 0;
                        tracer.Trace("Into  GovEntities after response " + ovrelateddoclist.Count());
                        foreach (var c in ovrelateddoclist)
                        {
                            i++;
                            try
                            {


                                Entity GovE = new Entity("govwin_subgoventites");
                                if (c.links.children.href != "" && c.links.children.href != null)
                                {
                                    GovE["govwin_children"] = (c.links.children.href);
                                }
                                if (c.id != 0)
                                {
                                    GovE["govwin_id"] = (c.id);
                                    GovE["govwin_id2"] = Convert.ToString(c.id);
                                }
                                if (c.market != "" && c.market != null)
                                {
                                    GovE["govwin_market"] = (c.market);
                                }
                                if (c.links.spendingProfile.href != "" && c.links.spendingProfile.href != null)
                                {
                                    GovE["govwin_spendingprofile"] = (c.links.spendingProfile.href);
                                }
                                if (c.title != "" && c.title != null)
                                {
                                    GovE["govwin_title"] = (c.title);
                                }

                                GovE["govwin_name"] = (i.ToString());
                                EntityReference erf = new EntityReference("govwin_govgov_entites", targetEntity.Id);
                                GovE["govwin_goventitylistid"] = erf;

                                GovEntityguid = service.Create(GovE);
                                if (c.parentHierarchy.Count > 0)
                                {

                                    foreach (var ph in c.parentHierarchy)
                                    {
                                        Guid phguid = new Guid();
                                        Entity perents = new Entity("govwin_goventityparenthierarchy");

                                        if (ph.id != null && ph.id != "")
                                        {
                                            perents["govwin_govid"] = Convert.ToString(c.id);
                                        }
                                        if (ph.title != null && ph.title != "")
                                        {
                                            perents["govwin_name"] = Convert.ToString(c.title);
                                            perents["govwin_govtitle"] = Convert.ToString(c.title);
                                        }
                                        EntityReference entref = new EntityReference("govwin_subgoventites", GovEntityguid);
                                        perents["govwin_goventitylookup"] = entref;
                                        phguid = service.Create(perents);
                                    }
                                }
                                tracer.Trace("Into  GovEntities after response added");
                                EntityReference mlt = new EntityReference();
                                mlt.LogicalName = "govwin_subgoventites";
                                mlt.Id = GovEntityguid;
                                ovrelateddocEntity.Add(mlt);
                            }
                            catch (Exception)
                            {
                                tracer.Trace("Into  GovEntities after response added erreor " );
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }
        }


        public List<GovEntities> getgoventities(string url, string token, ITracingService trace)
        {
            List<GovEntities> GovEntitieslist = new List<GovEntities>();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers["Authorization"] = token;
                request.Method = "GET";
                request.KeepAlive = false;
                try
                {
                    WebResponse response = request.GetResponse();
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        var json = JsonConvert.DeserializeObject<RootObjectEntities>(result);
                        GovEntitieslist = json.govEntities;
                    }
                    response.Dispose();
                }
                catch (Exception)
                { }

            }
            catch (Exception)
            {

            }

            return GovEntitieslist;
        }

    }
    public class ParentHierarchy
    {
        public string id { get; set; }
        public string title { get; set; }
    }

    public class GovEntities
    {
        public Linksentities links { get; set; }
        public string title { get; set; }
        public int id { get; set; }
        public string market { get; set; }
        public List<ParentHierarchy> parentHierarchy { get; set; }
    }

    public class RootObjectEntities
    {
        public Links links { get; set; }
        public List<GovEntities> govEntities { get; set; }
    }
    public class Linksentities
    {
        public Children children { get; set; }
        public SpendingProfile spendingProfile { get; set; }
    }
    public class Children
    {
        public string href { get; set; }
    }

    public class SpendingProfile
    {
        public string href { get; set; }
    }
}
