﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class fboNoticeApi : IPlugin
    {

        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            if (context.Depth == 2)
            {
                return;
            }
            try
            {
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    tracer.Trace("Into GovWin plugin to call api FBO");
                    //var targetEntity = (Entity)context.InputParameters["Target"];
                    //if (targetEntity.LogicalName != "govwin_govfbo_notices")
                    //    return;
                    ////Entity fboEntity = service.Retrieve("govwin_govfbo_notices", targetEntity.Id, new ColumnSet(true));
                    //Entity fboEntity = (Entity)context.InputParameters["Target"];
                    var opportunity = (Entity)context.InputParameters["Target"];
                    var targetEntity = opportunity.GetAttributeValue<EntityReference>("govwin_govwinfbonotices");
                    Entity fboEntity = service.Retrieve("govwin_govfbo_notices", targetEntity.Id, new ColumnSet(new string[] { "govwin_token", "govwin_href" }));
                    if (fboEntity.Attributes.Contains("govwin_token") && fboEntity.Attributes.Contains("govwin_href"))
                    {
                        tracer.Trace("Into  FBO token " + fboEntity.Attributes["govwin_token"].ToString());
                        tracer.Trace("Into  FBO link " + fboEntity.Attributes["govwin_href"].ToString());
                        var ovrelateddoclist = getFBOnotices(fboEntity.Attributes["govwin_href"].ToString(), fboEntity.Attributes["govwin_token"].ToString(), tracer);
                        EntityReferenceCollection ovrelateddocEntity = new EntityReferenceCollection();
                        Guid FBOguid = new Guid();
                        int i = 0;
                        tracer.Trace("Into  FBO after response " + ovrelateddoclist.Count());
                        foreach (var c in ovrelateddoclist)
                        {
                            i++;
                            try
                            {
                                //  fbon["govwin_fbonoticesname"] = c.

                                Entity fbon = new Entity("govwin_subgovfbonotices");

                                if (c.documentLink != "" && c.documentLink != null)
                                {
                                    fbon["govwin_documentlink"] = (c.documentLink);
                                }
                                if (c.id != "" && c.id != null)
                                {
                                    fbon["govwin_id"] = Convert.ToInt32(c.id);
                                    fbon["govwin_id2"] = (c.id);
                                }
                                if (c.publicationDate != "" && c.publicationDate != null)
                                {
                                    fbon["govwin_publicationdate"] = Convert.ToDateTime(c.publicationDate);
                                }
                                if (c.title != "" && c.title != null)
                                {
                                    fbon["govwin_title"] = (c.title);
                                }

                                fbon["govwin_name"] = (i.ToString());
                                EntityReference erf = new EntityReference("govwin_govfbo_notices", targetEntity.Id);
                                fbon["govwin_fbonoticelistid"] = erf;

                                FBOguid = service.Create(fbon);
                                tracer.Trace("Into  FBO after response added");
                                EntityReference CnTr = new EntityReference();
                                CnTr.LogicalName = "govwin_subgovfbonotices";
                                CnTr.Id = FBOguid;
                                ovrelateddocEntity.Add(CnTr);
                            }
                            catch (Exception)
                            {
                                tracer.Trace("Into  FBO after response added erreor ");
                            }
                        }

                    }

                }
            }
            catch (Exception)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }
        }



        public List<DocumentLinks> getFBOnotices(string url, string token, ITracingService trace)
        {


            List<DocumentLinks> GovEntitieslist = new List<DocumentLinks>();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers["Authorization"] = token;
                request.Method = "GET";
                request.KeepAlive = false;
                try
                {
                    WebResponse response = request.GetResponse();

                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        // var json1 = JsonConvert.DeserializeObject<IDictionary<string, RootObjectFBO>>(result);
                        //List<RootObjectFBO> datalist = JsonConvert.DeserializeObject<List<RootObjectFBO>>(result);

                        var json = JsonConvert.DeserializeObject<RootObjectFBO>(result);
                        GovEntitieslist = json.documentLinks;
                    }
                    response.Dispose();
                }
                catch (Exception)
                { }

            }
            catch (Exception)
            {

            }

            return GovEntitieslist;
        }


    }

    //FBO Notices Model

    public class DocumentLinks
    {
        public string documentLink { get; set; }
        public string id { get; set; }
        public string publicationDate { get; set; }
        public string title { get; set; }
    }

    public class RootObjectFBO
    {
        public Links links { get; set; }
        public Meta meta { get; set; }
        public List<DocumentLinks> documentLinks { get; set; }///confusing
    }

}
