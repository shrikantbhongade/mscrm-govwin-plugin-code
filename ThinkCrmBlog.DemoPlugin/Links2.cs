﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class Links2
    {
        public WebHref webHref { get; set; }
        public Contacts contacts { get; set; }
        public RelatedDocuments relatedDocuments { get; set; }
        public GovEntity govEntity { get; set; }
        public FboNotices fboNotices { get; set; }
        public Contracts contracts { get; set; }
        public Milestones milestones { get; set; }
        public Companies companies { get; set; }
        public RelatedArticles relatedArticles { get; set; }
        public PlacesOfPerformance placesOfPerformance { get; set; }
        public ContractVehicles contractVehicles { get; set; }
    }
}
