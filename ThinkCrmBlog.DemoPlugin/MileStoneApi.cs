﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class MileStoneApi : IPlugin
    {

        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            if (context.Depth == 2)
            {
                return;
            }

            try
            {
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    tracer.Trace("Into GovWin plugin to call api Milestone");
                    //var targetEntity = (Entity)context.InputParameters["Target"];
                    //if (targetEntity.LogicalName != "govwin_govmilestones")
                    //    return;
                    ////Entity milestone = service.Retrieve("govwin_govmilestones", targetEntity.Id, new ColumnSet(true));
                    //Entity milestone = (Entity)context.InputParameters["Target"]; // App Source
                    var opportunity = (Entity)context.InputParameters["Target"];
                    var targetEntity = opportunity.GetAttributeValue<EntityReference>("govwin_govwinmilestones");
                    Entity milestone = service.Retrieve("govwin_govmilestones", targetEntity.Id, new ColumnSet(new string[] { "govwin_token", "govwin_href" }));
                    if (milestone.Attributes.Contains("govwin_token") && milestone.Attributes.Contains("govwin_href"))
                    {
                        var ovrelateddoclist = getmilestoness(milestone.Attributes["govwin_href"].ToString(), milestone.Attributes["govwin_token"].ToString(), tracer);
                        EntityReferenceCollection ovrelateddocEntity = new EntityReferenceCollection();
                        Guid milestonesguid = new Guid();
                        int i = 0;
                        foreach (var c in ovrelateddoclist)
                        {
                            i++;
                            try
                            {

                                Entity miletype = new Entity("govwin_subgovmilestones");
                                if (c.deltekEstimate != "" && c.deltekEstimate != null)
                                {
                                    miletype["govwin_deltekestimatebool"] = Convert.ToBoolean(c.deltekEstimate);
                                }
                                if (c.govtEstimate != "" && c.govtEstimate != null)
                                {
                                    miletype["govwin_govtestimatebool"] = Convert.ToBoolean(c.govtEstimate);
                                }
                                if (c.mileStoneDate != "" && c.mileStoneDate != null)
                                {
                                    miletype["govwin_milestonedate"] = Convert.ToDateTime(c.mileStoneDate);
                                }
                                if (c.labelName != "" && c.labelName != null)
                                {
                                    miletype["govwin_milestones"] = (c.labelName);
                                }

                                miletype["govwin_name"] = (i.ToString());
                                EntityReference erf = new EntityReference("govwin_govmilestones", targetEntity.Id);
                                miletype["govwin_milestonelistid"] = erf;

                                milestonesguid = service.Create(miletype);
                                EntityReference mlt = new EntityReference();
                                mlt.LogicalName = "govwin_subgovmilestones";
                                mlt.Id = milestonesguid;
                                ovrelateddocEntity.Add(mlt);
                            }
                            catch (Exception)
                            {
                            }
                        }

                    }

                }
            }
            catch (Exception)
            {
            }
        }


        public List<Milestone> getmilestoness(string url, string token, ITracingService trace)
        {
            List<Milestone> Milestonelist = new List<Milestone>();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers["Authorization"] = token;
                request.Method = "GET";
                request.KeepAlive = false;
                try
                {
                    WebResponse response = request.GetResponse();

                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        var json = JsonConvert.DeserializeObject<RootObjectMilstone>(result);
                        Milestonelist = json.milestones;
                    }
                    response.Dispose();
                }
                catch (Exception)
                { }

            }
            catch (Exception)
            {

            }

            return Milestonelist;
        }

    }

    public class Milestone
    {
        public string deltekEstimate { get; set; }
        public string govtEstimate { get; set; }
        public string labelName { get; set; }
        public string mileStoneDate { get; set; }
    }

    public class RootObjectMilstone
    {
        public Links links { get; set; }
        public Meta meta { get; set; }
        public List<Milestone> milestones { get; set; }
    }
}
