﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class ContractVehiclesApi : IPlugin
    {

        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            if (context.Depth == 2)
            {
                return;
            }
            try
            {
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    tracer.Trace("Into GovWin plugin to call api ContractVehiclesApi");
                    //var targetEntity = (Entity)context.InputParameters["Target"];
                    //if (targetEntity.LogicalName != "govwin_govcontractvehicles")
                    //    return;
                    ////Entity CVehicles = service.Retrieve("govwin_govcontractvehicles", targetEntity.Id, new ColumnSet(true));
                    //Entity CVehicles = (Entity)context.InputParameters["Target"];
                    var opportunity = (Entity)context.InputParameters["Target"];
                    var targetEntity = opportunity.GetAttributeValue<EntityReference>("govwin_govwincontractvehicles");
                    Entity CVehicles = service.Retrieve("govwin_govcontractvehicles", targetEntity.Id, new ColumnSet(new string[] { "govwin_token", "govwin_href" }));

                    if (CVehicles.Attributes.Contains("govwin_token") && CVehicles.Attributes.Contains("govwin_href"))
                    {
                        tracer.Trace("Into  ContractVehiclesApi token " + CVehicles.Attributes["govwin_token"].ToString());
                        tracer.Trace("Into  ContractVehiclesApi link " + CVehicles.Attributes["govwin_href"].ToString());
                        var ovrelateddoclist = getCVehicles(CVehicles.Attributes["govwin_href"].ToString(), CVehicles.Attributes["govwin_token"].ToString(), tracer);
                        EntityReferenceCollection ovrelateddocEntity = new EntityReferenceCollection();
                        Guid CVGuid = new Guid();
                        int i = 0;
                        tracer.Trace("Into  ContractVehiclesApi after response " + ovrelateddoclist.Count());
                        foreach (var c in ovrelateddoclist)
                        {
                            i++;
                            try
                            {
                                tracer.Trace("Into  ContractVehiclesApi after response 1");

                                Entity CV = new Entity("govwin_subgovcontractvehicles");
                                tracer.Trace("Into  ContractVehiclesApi after response 2");
                                if (c.id != null)
                                {
                                    CV["govwin_govid"] = Convert.ToInt32(c.id);
                                    CV["govwin_govid2"] = Convert.ToString(c.id);
                                    tracer.Trace("Into  ContractVehiclesApi after response 3 and c.id = " + c.id);
                                }
                                if (c.title != "" && c.title != null)
                                {
                                    CV["govwin_title"] = (c.title);
                                    CV["govwin_name"] = (c.title);
                                    tracer.Trace("Into  ContractVehiclesApi after response 4");
                                }
                                if (c.type != "" && c.type != null)
                                {
                                    CV["govwin_type"] = (c.type);
                                    tracer.Trace("Into  ContractVehiclesApi after response 5");
                                }
                                if (c.subclasses != null)
                                {
                                    if (c.subclasses.id != null)
                                    {
                                        CV["govwin_subclassesid"] = Convert.ToInt32(c.subclasses.id);
                                        CV["govwin_subclassid2"] = Convert.ToString(c.subclasses.id);
                                        tracer.Trace("Into  ContractVehiclesApi after response 6");
                                    }
                                    if (c.subclasses.title != "" && c.subclasses.title != null)
                                    {
                                        CV["govwin_subclassestitle"] = (c.subclasses.title);
                                        tracer.Trace("Into  ContractVehiclesApi after response 7");
                                    }
                                    if (c.subclasses.number != "" && c.subclasses.number != null)
                                    {
                                        CV["govwin_subclassesnumber"] = (c.subclasses.number);
                                        tracer.Trace("Into  ContractVehiclesApi after response 8");
                                    }
                                }


                                EntityReference erf = new EntityReference("govwin_govcontractvehicles", targetEntity.Id);
                                tracer.Trace("Into  ContractVehiclesApi after response 9");
                                CV["govwin_contractvehiclelookup"] = erf;
                                tracer.Trace("Into  ContractVehiclesApi after response 10");

                                //CVGuid = service.Create(CV);
                                tracer.Trace("Into  ContractVehiclesApi after response 11");

                                tracer.Trace("Into  Contract Vehicles after response added");



                            }
                            catch (Exception ex)
                            {
                                tracer.Trace("Into  Contract Vehicles after response added erreor " + ex);
                            }
                        }

                    }

                }
            }
            catch (Exception)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }
        }

        public List<ContractVehicle> getCVehicles(string url, string token, ITracingService trace)
        {
            List<ContractVehicle> contractslist = new List<ContractVehicle>();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers["Authorization"] = token;
                request.Method = "GET";
                request.KeepAlive = false;
                try
                {
                    WebResponse response = request.GetResponse();
                    trace.Trace("Into contract got response ");
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        trace.Trace(result = result.ToString());
                        var json = JsonConvert.DeserializeObject<RootObjectContractVehicles>(result);
                        contractslist = json.contractVehicles;
                        trace.Trace("Into contract got serialize ");
                    }
                    response.Dispose();
                }
                catch (Exception)
                {
                    trace.Trace("Into contract erroer inner " );
                }

            }
            catch (Exception )
            {
                trace.Trace("Into contract erroer outer " );
            }

            return contractslist;
        }

    }

    public class subclasses
    {
        public string title { get; set; }
        public string number { get; set; }
        public int id { get; set; }
    }
    public class ContractVehicle
    {
        public subclasses subclasses { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public int id { get; set; }
    }

    public class RootObjectContractVehicles
    {
        public Links links { get; set; }
        public Meta meta { get; set; }
        public List<ContractVehicle> contractVehicles { get; set; }
    }

    //public class PlacesOfPerformance
    //{
    //    public string country { get; set; }
    //    public string isPrimary { get; set; }
    //}
    //public class RootObject
    //{
    //    public Links links { get; set; }
    //    public Meta meta { get; set; }
    //    public List<PlacesOfPerformance> placesOfPerformance { get; set; }
    //}
}
