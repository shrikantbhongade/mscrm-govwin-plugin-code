﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class CompaniesApi : IPlugin
    {

        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            if (context.Depth == 2)
            {
                return;
            }
            try
            {
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    tracer.Trace("Into GovWin plugin to call api companies");
                    //var targetEntity = (Entity)context.InputParameters["Target"];
                    //if (targetEntity.LogicalName != "govwin_govcompanies")
                    //    return;
                    ////Entity companys = service.Retrieve("govwin_govcompanies", targetEntity.Id, new ColumnSet(true));
                    ////Entity companys = service.Retrieve("govwin_govcompanies", targetEntity.Id, new ColumnSet(new string[] { "govwin_token", "govwin_token", "govwin_href" })); App Source
                    //Entity companys = (Entity)context.InputParameters["Target"];
                    var opportunity = (Entity)context.InputParameters["Target"];
                    var targetEntity = opportunity.GetAttributeValue<EntityReference>("govwin_govwincompanies");
                    Entity companys = service.Retrieve("govwin_govcompanies", targetEntity.Id, new ColumnSet(new string[] { "govwin_token", "govwin_href" }));

                    if (companys.Attributes.Contains("govwin_token") && companys.Attributes.Contains("govwin_href"))
                    {
                        tracer.Trace("Into  company token " + companys.Attributes["govwin_token"].ToString());
                        tracer.Trace("Into  company link " + companys.Attributes["govwin_href"].ToString());
                        var ovrelateddoclist = getcompanies(companys.Attributes["govwin_href"].ToString(), companys.Attributes["govwin_token"].ToString(), tracer);
                        EntityReferenceCollection ovrelateddocEntity = new EntityReferenceCollection();
                        Guid companyguid = new Guid();
                        int i = 0;
                        tracer.Trace("Into  company after response " + ovrelateddoclist.Count());
                        foreach (var c in ovrelateddoclist)
                        {
                            i++;
                            try
                            {

                                Entity company = new Entity("govwin_subgovcompanies");
                                if (c.id != null)
                                {
                                    company["govwin_govid"] = Convert.ToInt32(c.id);
                                    company["govwin_govid2"] = Convert.ToString(c.id);
                                }
                                if (c.name != "" && c.name != null)
                                {
                                    company["govwin_name"] = (c.name);
                                }

                                EntityReference erf = new EntityReference("govwin_govcompanies", targetEntity.Id);
                                company["govwin_companieslookup"] = erf;

                                companyguid = service.Create(company);
                                tracer.Trace("Into  company after response added");



                            }
                            catch (Exception)
                            {
                                tracer.Trace("Into  company after response added erreor ");
                            }
                        }

                    }

                }
            }
            catch (Exception)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }
        }

        public List<Company> getcompanies(string url, string token, ITracingService trace)
        {
            List<Company> contractslist = new List<Company>();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers["Authorization"] = token;
                request.Method = "GET";
                request.KeepAlive = false;
                try
                {
                    WebResponse response = request.GetResponse();
                    trace.Trace("Into company got response ");
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        var json = JsonConvert.DeserializeObject<RootObjectCompanies>(result);
                        contractslist = json.companies;
                        trace.Trace("Into company got serialize ");
                    }
                    response.Dispose();//AppSource
                }
                catch (Exception exe)
                {
                    trace.Trace("Into company erroer inner " + exe.Message);
                }

            }
            catch (Exception exe)
            {
                trace.Trace("Into company erroer outer " + exe.Message);
            }

            return contractslist;
        }

    }

    public class RootObjectCompanies
    {
        public Links links { get; set; }
        public Meta meta { get; set; }
        public List<Company> companies { get; set; }
    }
}
