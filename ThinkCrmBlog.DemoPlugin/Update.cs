﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class Update : IPlugin
    {
        public class SendData
        {
            public string DisplayName { get; set; }
            public string SchemaName { get; set; }
        }
        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            tracer.Trace("Into GovWin plugin to call api schedule");
            serviceProvider.GetService(typeof(Microsoft.Xrm.Sdk.IPluginExecutionContext));

            try

            {
                if (context.InputParameters.Contains("Target") &&
                                    context.InputParameters["Target"] is Entity)
                {
                    var targetEntity = (Entity)context.InputParameters["Target"];
                    if (targetEntity.LogicalName != "govwin_govupdates")
                        return;

                    if (targetEntity.Attributes.Contains("govwin_opportunitylookup"))
                    {
                        tracer.Trace("have look up schedule");

                        var prreff = targetEntity.GetAttributeValue<EntityReference>("govwin_opportunitylookup");



                        //Entity Opportunity = service.Retrieve("opportunity", prreff.Id, new ColumnSet(true));
                        Entity Opportunity = (Entity)context.InputParameters["Target"];
                        string fedcap_opportunitystatus = string.Empty;
                        if (Opportunity.Attributes.Contains("fedcap_opportunitystatus"))
                        {
                            fedcap_opportunitystatus = Opportunity.FormattedValues["fedcap_opportunitystatus"].ToString();
                        }
                        if (!fedcap_opportunitystatus.Equals("Closed/Lost") || !fedcap_opportunitystatus.Equals("Closed/Won"))
                        {
                            var res = getopportunity(service, Opportunity, tracer);
                            tracer.Trace("Into GovWin plugingot Result");
                            if (res.Count > 0)
                            {
                                foreach (var oplist in res)
                                {
                                    postopprtunity(oplist, service, tracer, Opportunity, context);
                                }
                            }
                        }
                        else
                        {
                            tracer.Trace("Note: Data can not update while stage as Closed Won/Lost");
                        }

                    }
                }
                //TODO: Do stuff
            }
            catch (Exception e)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }
        }

        public List<Opportunity> getopportunity(IOrganizationService service, Entity Opportunity, ITracingService tracer)
        {
            List<Opportunity> opplist = new List<Opportunity>();
            try
            {
                var QECredential = new QueryExpression("govwin_govcredentials");
                QECredential.NoLock = true;
                //QECredential.ColumnSet = new ColumnSet(true);
                QECredential.ColumnSet = new ColumnSet(new string[] { "govwin_clientid", "govwin_clientsecret", "govwin_endpoint", "govwin_password", "govwin_username" });
                var AllCreds = service.RetrieveMultiple(QECredential).Entities;
                //Opportunity op = new Opportunity();
                govWin govWin = new govWin();
                //string ur = "";
                //Guid gd = new Guid();
                if (Opportunity.Attributes.Contains("govwin_govwin_opportunity_id"))
                {
                    govWin.OppId = Opportunity.Attributes["govwin_govwin_opportunity_id"].ToString();
                }

                var cases = AllCreds[0];
                var item = cases.Attributes.Keys;
                //foreach (var a in item)
                //{
                //    var data = cases.Attributes[a];
                //    if (a == "govwin_clientid")
                //    {
                //        govWin.Client_Id__c = data.ToString();
                //    }
                //    else if (a == "govwin_clientsecret")
                //    {
                //        govWin.Client_Secret__c = data.ToString();
                //    }

                //    else if (a == "govwin_endpoint")
                //    {
                //        govWin.End_Point__c = data.ToString();
                //    }
                //    else if (a == "govwin_password")
                //    {
                //        govWin.Password__c = data.ToString();
                //    }
                //    else if (a == "govwin_username")
                //    {
                //        govWin.User_Name__c = data.ToString();
                //    }
                //}
                govWin.Client_Id__c = Convert.ToString(cases.Attributes["govwin_clientid"]);
                govWin.Client_Secret__c = Convert.ToString(cases.Attributes["govwin_clientsecret"]);
                govWin.End_Point__c = Convert.ToString(cases.Attributes["govwin_endpoint"]);
                govWin.Password__c = Convert.ToString(cases.Attributes["govwin_password"]);
                govWin.User_Name__c = Convert.ToString(cases.Attributes["govwin_username"]);
                tracer.Trace("Into GovWin plugingot Result credentials ");

                var urldata = "https://services.govwin.com/neo-ws/oauth/token?" + "client_id=" + govWin.Client_Id__c + "&client_secret=" + govWin.Client_Secret__c + "&grant_type=password&username=" + govWin.User_Name__c + "&password=" + govWin.Password__c + "&scope=read";

                var request2 = (HttpWebRequest)HttpWebRequest.Create(urldata);
                request2.Headers.Add("x-ms-version", "2012-08-01"); request2.Method =
                "POST"; request2.ContentType = "application/json";
                request2.Method = "POST";
                request2.KeepAlive = false;
                Stream dataStream = request2.GetRequestStream();
                WebResponse response2 = request2.GetResponse();
                dataStream = response2.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                using (var streamReader = new StreamReader(response2.GetResponseStream()))
                {
                    var resultss = streamReader.ReadToEnd();

                    try
                    {
                        var json = JsonConvert.DeserializeObject<TokenClass>(resultss);
                        govWin.Access_Token__c = json.access_token;
                        govWin.Refresh_Token__c = json.refresh_token;
                    }
                    catch (WebException ex)
                    { }

                    //tracer.Trace("get opportunity got response into stream reader opplist " + opplist.Count);
                }
                //WebRequest req = (HttpWebRequest)WebRequest.Create(@"https://services.govwin.com/neo-ws/oauth/token?" + "client_id=" + govWin.Client_Id__c + "&client_secret=" + govWin.Client_Secret__c + "&grant_type=password&username=" + govWin.User_Name__c + "&password=" + govWin.Password__c + "&scope=read");
                //var client = new RestClient(govWin.End_Point__c + "?" + "client_id=" + govWin.Client_Id__c + "&client_secret=" + govWin.Client_Secret__c + "&grant_type=password&username=" + govWin.User_Name__c + "&password=" + govWin.Password__c + "&scope=read");
                //var request = new RestRequest(Method.POST);
                //IRestResponse response = client.Execute(request);
                //var token = response.Content.ToString();
                //Authtoken = response.Content.ToString();
                //dynamic d = JObject.Parse(token);
                // var  TokObject = JsonConvert.DeserializeObject<TokenClass>(dataStream);
                // govWin.Access_Token__c = TokObject.access_token;
                //  govWin.Refresh_Token__c = TokObject.refresh_token;
                tracer.Trace("get opportunity token refresh " + govWin.Refresh_Token__c);
                tracer.Trace("get opportunity token access " + govWin.Access_Token__c);
                // govWin.OppId = "OPP138275";
                var url = "https://services.govwin.com/neo-ws/opportunities/OPP" + govWin.OppId;
                try
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                    var request1 = (HttpWebRequest)WebRequest.Create(url);
                    //request1.Host = "iq.qa1.govwin.com";
                    request1.Headers["Authorization"] = "bearer" + govWin.Access_Token__c;
                    request1.Headers["Accept-Encoding"] = "gzip,deflate";
                    request1.Method = "GET";
                    acessToken = "bearer" + govWin.Access_Token__c;
                    try
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        request1.KeepAlive = false;
                        WebResponse response1 = request1.GetResponse();
                        using (var streamReader = new StreamReader(response1.GetResponseStream()))
                        {
                            var result = streamReader.ReadToEnd();
                            var json = JsonConvert.DeserializeObject<RootObject>(result);
                            opplist = json.opportunities;
                            tracer.Trace("get opportunity got response into stream reader opplist " + opplist.Count);
                        }
                        response1.Dispose();
                    }
                    catch (Exception exe)
                    {
                        tracer.Trace("get opportunity error got response into stream reader");
                    }
                    response2.Dispose();
                }
                catch (Exception exe)
                {
                    tracer.Trace("get opportunity error got response into stream reader");
                }
            }

            catch (Exception ex)
            {
                tracer.Trace("get opportunity errorouter url");
            }

            return opplist;
        }

        public bool postopprtunity(Opportunity op, IOrganizationService service, ITracingService tracer, Entity Opportunities, IPluginExecutionContext context)
        {
            bool update = false;
            tracer.Trace("Into GovWin postopprtunity");
            try
            {
                Entity UpdateCount = new Entity("govwin_subgovupdates");
                try
                {
                    if (op.solicitationNumber != "" && op.solicitationNumber != null)
                    {
                        if (Opportunities.Attributes["govwin_solicitation_number"].ToString() == op.solicitationNumber)
                        {

                        }
                        else
                        {
                            update = true;
                            UpdateCount["govwin_solicitationnumber"] = Opportunities.Attributes["govwin_solicitation_number"].ToString();
                            Opportunities["govwin_solicitation_number"] = op.solicitationNumber;
                        }

                    }
                }
                catch (Exception ex)
                {
                }
                try
                {
                    if (op.solicitationDate != null)
                    {

                        if (op.solicitationDate.value != "" && op.solicitationDate.value != null)
                        {
                            string Formatdate = op.solicitationDate.value.Replace("T", " ");
                            if (Opportunities.Attributes["govwin_solicitation_date_value"].ToString() == Formatdate)
                            {

                            }
                            else
                            {
                                update = true;
                                UpdateCount["govwin_solicitationdate"] = Opportunities.Attributes["govwin_solicitation_date_value"].ToString();
                                Opportunities["govwin_solicitation_date_value"] = Formatdate;
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                }
                try
                {
                    if (op.updateDate != "" && op.updateDate != null)
                    {
                        string Formatdate = op.updateDate.Replace("T", " ");
                        if (Opportunities.Attributes["govwin_update_date"].ToString() == Formatdate)
                        {

                        }
                        else
                        {
                            update = true;
                            UpdateCount["govwin_updatedate"] = Opportunities.Attributes["govwin_update_date"].ToString();
                            Opportunities["govwin_update_date"] = Formatdate;
                        }

                    }
                }
                catch (Exception ex)
                {
                }
                try
                {
                    if (op.typeOfAward != "" && op.typeOfAward != null)
                    {
                        if (Opportunities.Attributes["govwin_type_of_award"].ToString() == op.typeOfAward)
                        {
                        }
                        else
                        {
                            update = true;
                            UpdateCount["govwin_typeofaward"] = Opportunities.Attributes["govwin_type_of_award"].ToString();
                            Opportunities["govwin_type_of_award"] = op.typeOfAward;
                        }

                    }
                }
                catch (Exception ex)
                {
                }
                try
                {
                    if (op.type != "" && op.type != null)
                    {
                        if (Opportunities.Attributes["govwin_type"].ToString() == op.type)
                        {

                        }
                        else
                        {
                            update = true;
                            UpdateCount["govwin_type"] = Opportunities.Attributes["govwin_type"].ToString();
                            Opportunities["govwin_type"] = op.type;
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                try
                {
                    if (op.title != "" && op.title != null)
                    {
                        if (Opportunities.Attributes["govwin_title"].ToString() == op.title)
                        {

                        }
                        else
                        {
                            update = true;
                            UpdateCount["govwin_title"] = Opportunities.Attributes["govwin_title"].ToString();
                            Opportunities["govwin_title"] = op.title;
                        }

                    }
                }
                catch (Exception ex)
                {
                }
                try
                {
                    if (op.status != "" && op.status != null)
                    {
                        if (Opportunities.Attributes["govwin_status"].ToString() == op.status)
                        {

                        }
                        else
                        {
                            update = true;
                            UpdateCount["govwin_status"] = Opportunities.Attributes["govwin_status"].ToString();
                            Opportunities["govwin_status"] = op.status;
                        }

                    }
                }
                catch (Exception ex) { tracer.Trace("Into Opportunity update govwin_status " + ex.Message); }
                try
                {
                    if (op.duration != "" && op.duration != null)
                    {
                        if (Opportunities.Attributes["govwin_duration"].ToString() == op.duration)
                        {

                        }
                        else
                        {
                            update = true;
                            UpdateCount["govwin_duration"] = Opportunities.Attributes["govwin_duration"].ToString();
                            Opportunities["govwin_duration"] = op.duration;
                        }

                    }
                }
                catch (Exception ex)
                {
                }
                try
                {
                    if (op.primaryRequirement != "" && op.primaryRequirement != null)
                    {
                        if (Opportunities.Attributes["govwin_primary_requirement"].ToString() == op.primaryRequirement)
                        {

                        }
                        else
                        {
                            update = true;
                            UpdateCount["govwin_primaryrequirement"] = Opportunities.Attributes["govwin_primary_requirement"].ToString();
                            Opportunities["govwin_primary_requirement"] = op.primaryRequirement;
                        }

                    }
                }
                catch (Exception ex)
                {
                }

                try
                {
                    if (op.oppValue != null)
                    {
                        if (Convert.ToInt32(Opportunities.Attributes["govwin_opportunity_value"]) == op.oppValue)
                        {

                        }
                        else
                        {
                            update = true;
                            UpdateCount["govwin_opportunityvalue"] = Opportunities.Attributes["govwin_opportunity_value"].ToString();
                            Opportunities["govwin_opportunity_value"] = Convert.ToString(op.oppValue);
                        }

                    }
                }
                catch (Exception ex)
                {
                }
                if (update == true)
                {
                    try
                    {
                        context.InputParameters["Target"] = Opportunities;
                        service.Update(Opportunities);
                    }
                    catch (Exception ex)
                    {
                    }
                    try
                    {
                        EntityReference er = new EntityReference("opportunity", Opportunities.Id);
                        UpdateCount["govwin_opportunitylookup"] = er;
                        service.Create(UpdateCount);
                    }
                    catch (Exception ex)
                    {
                    }

                }



            }
            catch (Exception ex1)
            {
            }
            return true;
        }

    }
}
