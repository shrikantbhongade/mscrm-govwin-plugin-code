﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class Opportunity
    {
        public Links2 links { get; set; }
        public PrimaryNAICS primaryNAICS { get; set; }
        public AwardDate awardDate { get; set; }
        public List<CompetitionType> competitionTypes { get; set; }
        public List<ContractType> contractTypes { get; set; }
        public string description { get; set; }
        public string duration { get; set; }
        public GovEntity2 govEntity { get; set; }
        public string id { get; set; }
        public int iqOppId { get; set; }
        public int oppValue { get; set; }
        public string primaryRequirement { get; set; }
        public string procurement { get; set; }
        public SolicitationDate solicitationDate { get; set; }
        public string solicitationNumber { get; set; }
        public string sourceURL { get; set; }
        public string status { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public string typeOfAward { get; set; }
        public string updateDate { get; set; }
        public int priority { get; set; }
        public string internalStatus { get; set; }


    }
}
