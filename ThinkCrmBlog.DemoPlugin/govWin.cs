﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class govWin
    {

        public string Client_Id__c { get; set; }
        public string Client_Secret__c { get; set; }
        public string User_Name__c { get; set; }
        public string Password__c { get; set; }
        public string End_Point__c { get; set; }
        public string Refresh_Token__c { get; set; }
        public string Access_Token__c { get; set; }
        public string OppId { get; set; }

    }
}
