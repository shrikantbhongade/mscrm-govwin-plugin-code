﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class ContactApi : IPlugin
    {

        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            if (context.Depth == 2)
            {
                return;
            }

            try
            {
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    tracer.Trace("Into GovWin plugin to call api contact");

                    //var targetEntity = (Entity)context.InputParameters["Target"];
                    //if (targetEntity.LogicalName != "govwin_govcontacts")
                    //    return;
                    ////Entity contacts = service.Retrieve("govwin_govcontacts", targetEntity.Id, new ColumnSet(true));
                    //Entity contacts = (Entity)context.InputParameters["Target"];
                    var opportunity = (Entity)context.InputParameters["Target"];
                    var targetEntity = opportunity.GetAttributeValue<EntityReference>("govwin_govwincontacts");
                    Entity contacts = service.Retrieve("govwin_govcontacts", targetEntity.Id, new ColumnSet(new string[] { "govwin_token", "govwin_href" }));

                    if (contacts.Attributes.Contains("govwin_token") && contacts.Attributes.Contains("govwin_href"))
                    {
                        tracer.Trace("Into  contact token " + contacts.Attributes["govwin_token"].ToString());
                        tracer.Trace("Into  contact link " + contacts.Attributes["govwin_href"].ToString());
                        var ovrelateddoclist = getcontacts(contacts.Attributes["govwin_href"].ToString(), contacts.Attributes["govwin_token"].ToString(), tracer);
                        EntityReferenceCollection ovrelateddocEntity = new EntityReferenceCollection();
                        Guid contactguid = new Guid();
                        int i = 0;
                        tracer.Trace("Into  contact after response " + ovrelateddoclist.Count());
                        foreach (var c in ovrelateddoclist)
                        {
                            i++;
                            try
                            {

                                Entity contptype = new Entity("govwin_subgovcontacts");
                                if (c.contactId != 0)
                                {
                                    contptype["govwin_contactid"] = (c.contactId);
                                    contptype["govwin_contactid2"] = Convert.ToString(c.contactId);
                                }
                                if (c.title != "" && c.title != null)
                                {
                                    contptype["govwin_title"] = (c.title);
                                }
                                if (c.firstName != "" && c.firstName != null && c.lastName != "" && c.lastName != null)
                                {
                                    contptype["govwin_contactname"] = c.firstName;
                                }
                                if (c.email != "" && c.email != null)
                                {
                                    contptype["govwin_email"] = c.email;
                                }
                                if (c.firstName != "" && c.firstName != null)
                                {
                                    contptype["govwin_firstname"] = (c.firstName);
                                }
                                if (c.lastName != "" && c.lastName != null)
                                {
                                    contptype["govwin_lastname"] = (c.lastName);
                                }
                                if (c.modifiedDate != "" && c.modifiedDate != null)
                                {
                                    contptype["govwin_modifieddate"] = Convert.ToDateTime(c.modifiedDate);
                                }
                                if (c.phone != "" && c.phone != null)
                                {
                                    contptype["govwin_phone"] = (c.phone);
                                }
                                contptype["govwin_name"] = (i.ToString());

                                //new field added by MK 
                                //govwin_address1 govwin_address2  govwin_city  govwin_zipcode  govwin_contacttype  govwin_goventitylevel1

                                if (c.address1 != "" && c.address1 != null)
                                {
                                    contptype["govwin_address1"] = (c.address1);
                                }
                                if (c.address2 != "" && c.address2 != null)
                                {
                                    contptype["govwin_address2"] = (c.address2);
                                }
                                if (c.city != "" && c.city != null)
                                {
                                    contptype["govwin_city"] = (c.city);
                                }
                                if (c.zip != "" && c.zip != null)
                                {
                                    contptype["govwin_zipcode"] = (c.zip);
                                }
                                if (c.contactType != "" && c.contactType != null)
                                {
                                    contptype["govwin_contacttype"] = (c.contactType);
                                }
                                if (c.govEntityLevel1 != "" && c.govEntityLevel1 != null)
                                {
                                    contptype["govwin_goventitylevel1"] = (c.govEntityLevel1);
                                }
                                if (c.govEntityLevel2 != "" && c.govEntityLevel2 != null)
                                {
                                    contptype["govwin_goventitylevel2"] = (c.govEntityLevel2);
                                }

                                EntityReference erf = new EntityReference("govwin_govcontacts", targetEntity.Id);
                                contptype["govwin_contactlookup"] = erf;

                                contactguid = service.Create(contptype);
                                tracer.Trace("Into  contact after response added");
                                //EntityReference CnTr = new EntityReference();
                                //CnTr.LogicalName = "govwin_subgovcontacts";
                                //CnTr.Id = contactguid;
                                //ovrelateddocEntity.Add(CnTr);


                            }
                            catch (Exception)
                            {
                                tracer.Trace("Into  contact after response added erreor ");
                            }
                        }
                        //if (ovrelateddocEntity.Count > 0)
                        //{
                        //    tracer.Trace("count of related count "+ ovrelateddocEntity.Count);
                        //    Relationship ovrelateddoc = new Relationship("govwin_govwin_govcontracts_govwin_subgovcontracts");
                        //    service.Associate("govwin_govcontracts", contracts.Id, ovrelateddoc, ovrelateddocEntity);
                        //    tracer.Trace("Into  contract after response association " );
                        //    ovrelateddocEntity = null;
                        //}
                    }

                }
                //TODO: Do stuff
            }
            catch (Exception)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }
        }

        public List<Contact> getcontacts(string url, string token, ITracingService trace)
        {
            List<Contact> contractslist = new List<Contact>();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers["Authorization"] = token;
                request.Method = "GET";
                request.KeepAlive = false;
                try
                {
                    WebResponse response = request.GetResponse();
                    trace.Trace("Into contract got response ");
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        var json = JsonConvert.DeserializeObject<RootObjectContact>(result);
                        contractslist = json.contacts;
                        trace.Trace("Into contract got serialize ");
                    }
                    response.Dispose();
                }
                catch (Exception )
                {
                    trace.Trace("Into contract erroer inner " );
                }

            }
            catch (Exception )
            {
                trace.Trace("Into contract erroer outer ");
            }

            return contractslist;
        }

    }
}
public class Contact
{
    public Links3 links { get; set; }
    public int contactId { get; set; }
    public string email { get; set; }
    public string firstName { get; set; }

    public string lastName { get; set; }
    public string modifiedDate { get; set; }
    public string phone { get; set; }
    public string title { get; set; }

    public string address1 { get; set; }
    public string address2 { get; set; }
    public string city { get; set; }
    public string zip { get; set; }
    public string contactType { get; set; }
    public string govEntityLevel1 { get; set; }
    public string govEntityLevel2 { get; set; }

}

public class RootObjectContact
{
    public Links links { get; set; }
    public Meta meta { get; set; }
    public List<Contact> contacts { get; set; }
}
public class Links3
{
    public GovEntity govEntity { get; set; }
}
public class Links
{
}
public class GovEntity
{
    public string href { get; set; }
}
public class Paging
{
    public int max { get; set; }
    public int offset { get; set; }
    public string order { get; set; }
    public string sort { get; set; }
    public int totalCount { get; set; }
}
public class Meta
{
    public Paging paging { get; set; }
}
