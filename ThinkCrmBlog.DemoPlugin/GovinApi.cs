﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class GovinApi : IPlugin
    {
        public class SendData
        {
            public string DisplayName { get; set; }
            public string SchemaName { get; set; }
        }
        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            if (context.Depth > 1)
            {
                return;
            }
            tracer.Trace("Into GovWin plugin to call api");
            try
            {
                Entity Opportunity1 = (Entity)context.InputParameters["Target"];
                //Entity Opportunity1 = null;
                //if (context.PreEntityImages.Contains("preImage") && context.PreEntityImages["preImage"] != null)
                //{
                //    Opportunity1 = (Entity)context.PreEntityImages["preImage"];
                //}
                string oppGuid = Opportunity1.Id.ToString();
                tracer.Trace("Opportunity1.Id:- " + Opportunity1.Id.ToString());
                Guid opG = new Guid(oppGuid);
                try
                {
                    Entity Opportunity = service.Retrieve("opportunity", opG, new ColumnSet(new string[] { "fedcap_opportunitystatus", "govwin_govwin_opportunity_id" }));
                    //Entity Opportunity = (Entity)context.InputParameters["Target"];
                    tracer.Trace("Into GovWin plugingot opportunity");
                    string fedcap_opportunitystatus = string.Empty;
                    if (Opportunity.Attributes.Contains("fedcap_opportunitystatus"))
                    {
                        fedcap_opportunitystatus = Opportunity.FormattedValues["fedcap_opportunitystatus"].ToString();
                    }
                    if (!fedcap_opportunitystatus.Equals("Closed/Lost") || !fedcap_opportunitystatus.Equals("Closed/Won"))
                    {
                        var res = getopportunity(service, Opportunity, tracer);
                        tracer.Trace("Into GovWin plugingot Result");
                        if (res != null && res.Count > 0)
                        {
                            foreach (var oplist in res)
                            {
                                postopprtunity(oplist, service, tracer, Opportunity, context);
                                Opportunity["fedcap_govwinstatus"] = "Successfull";
                                context.InputParameters["Target"] = Opportunity; // App Source
                                service.Update(Opportunity);
                                return;
                            }
                        }
                        else
                        {
                            tracer.Trace("Fail 1");
                            Opportunity["fedcap_govwinstatus"] = "fail";
                            context.InputParameters["Target"] = Opportunity; // App Source
                            service.Update(Opportunity);
                            return;
                        }
                    }
                }
                catch (Exception)
                {
                    // when fedcap_opportunitystatus not exist
                    tracer.Trace("when fedcap_opportunitystatus not exist");
                    Entity Opportunity = service.Retrieve("opportunity", opG, new ColumnSet(new string[] { "govwin_govwin_opportunity_id" }));
                    //Entity Opportunity = (Entity)context.InputParameters["Target"];
                    tracer.Trace("Into GovWin plugingot opportunity");
                    var res = getopportunity(service, Opportunity, tracer);
                    tracer.Trace("Into GovWin plugingot Result");
                    if (res != null && res.Count > 0)
                    {
                        foreach (var oplist in res)
                        {
                            postopprtunity(oplist, service, tracer, Opportunity, context);
                            Opportunity["fedcap_govwinstatus"] = "Successfull";
                            context.InputParameters["Target"] = Opportunity; // App Source
                            service.Update(Opportunity);
                            return;
                        }
                    }
                    else
                    {
                        tracer.Trace("Fail 1");
                        Opportunity["fedcap_govwinstatus"] = "fail";
                        context.InputParameters["Target"] = Opportunity; // App Source
                        service.Update(Opportunity);
                        return;
                    }
                }
                
            }
            catch (Exception)
            {
            }
           


            //ExecuteMultipleRequest requestWithResults = new ExecuteMultipleRequest()
            //{
            //    Settings = new ExecuteMultipleSettings()
            //    {
            //        ContinueOnError = false,
            //        ReturnResponses = false
            //    },
            //    Requests = new OrganizationRequestCollection()
            //};

            //tracer.Trace(context.InputParameters["Query"].ToString());
            //try
            //{
            //    if (context.InputParameters.Contains("Query") && context.PrimaryEntityName == "incident" && context.InputParameters["Query"] is QueryExpression)
            //    {
            //        QueryExpression qe = (QueryExpression)context.InputParameters["Query"];
            //        qe.NoLock = true;
            //        if (qe.Criteria.Conditions.Count > 0)
            //        {
            //            if (qe.Criteria.Conditions[1].AttributeName == "title" && qe.Criteria.Conditions[1].Values[0].ToString() == "ImtiyazPlugin")
            //            {
            //                if (qe.Criteria.Conditions[0].AttributeName == "description")
            //                {
            //                    string oppGuid = qe.Criteria.Conditions[0].Values[0].ToString();
            //                    Guid opG = new Guid(oppGuid);
            //                    Entity Opportunity = service.Retrieve("opportunity", opG, new ColumnSet(new string[] { "fedcap_opportunitystatus", "govwin_govwin_opportunity_id" }));
            //                    //Entity Opportunity = (Entity)context.InputParameters["Target"];
            //                    tracer.Trace("Into GovWin plugingot opportunity");
            //                    string fedcap_opportunitystatus = string.Empty;
            //                    if (Opportunity.Attributes.Contains("fedcap_opportunitystatus"))
            //                    {
            //                        fedcap_opportunitystatus = Opportunity.FormattedValues["fedcap_opportunitystatus"].ToString();
            //                    }
            //                    if (!fedcap_opportunitystatus.Equals("Closed/Lost") || !fedcap_opportunitystatus.Equals("Closed/Won"))
            //                    {
            //                        var res = getopportunity(service, Opportunity, tracer);
            //                        tracer.Trace("Into GovWin plugingot Result");
            //                        if (res != null && res.Count > 0)
            //                        {
            //                            foreach (var oplist in res)
            //                            {
            //                                postopprtunity(oplist, service, tracer, Opportunity, context);
            //                                Opportunity["fedcap_govwinstatus"] = "Successfull";
            //                                context.InputParameters["Target"] = Opportunity; // App Source
            //                                service.Update(Opportunity);
            //                                return;
            //                            }
            //                        }
            //                        else
            //                        {
            //                            tracer.Trace("Fail 1");
            //                            Opportunity["fedcap_govwinstatus"] = "fail";
            //                            context.InputParameters["Target"] = Opportunity; // App Source
            //                            service.Update(Opportunity);
            //                            return;
            //                        }
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                tracer.Trace("Fail 2");
            //                return;
            //            }
            //        }
            //    }
            //    else
            //    {
            //        tracer.Trace("Fail 4");
            //    }
            //    //TODO: Do stuff
            //}
            //catch (Exception e)
            //{
            //    tracer.Trace("Fail 3");
            //}
        }

        public List<Opportunity> getopportunity(IOrganizationService service, Entity Opportunity, ITracingService tracer)
        {
            List<Opportunity> opplist = new List<Opportunity>();
            try
            {
                var QECredential = new QueryExpression("govwin_govcredentials");
                QECredential.NoLock = true;
                QECredential.ColumnSet = new ColumnSet(new string[] { "govwin_clientid", "govwin_clientsecret", "govwin_endpoint", "govwin_password", "govwin_username" });
                var AllCreds = service.RetrieveMultiple(QECredential).Entities;
                
                tracer.Trace("AllCreds.Count:- " + AllCreds.Count.ToString());
                govWin govWin = new govWin();
                if (Opportunity.Attributes.Contains("govwin_govwin_opportunity_id"))
                {
                    govWin.OppId = Opportunity.Attributes["govwin_govwin_opportunity_id"].ToString();
                    tracer.Trace("govWin.OppId:-" + govWin.OppId);
                }
                else { tracer.Trace("govWin.OppId:-" + govWin.OppId); return null; }
                var cases = AllCreds[0];
                tracer.Trace("Into GovWin plugingot Result credentials ");
                var item = cases.Attributes.Keys;
                //foreach (var a in item)
                //{
                //    var data = cases.Attributes[a];
                //    if (a == "govwin_clientid")
                //    {
                //        govWin.Client_Id__c = data.ToString();
                //    }
                //    else if (a == "govwin_clientsecret")
                //    {
                //        govWin.Client_Secret__c = data.ToString();
                //    }

                //    else if (a == "govwin_endpoint")
                //    {
                //        govWin.End_Point__c = data.ToString();
                //    }
                //    else if (a == "govwin_password")
                //    {
                //        govWin.Password__c = data.ToString();
                //    }
                //    else if (a == "govwin_username")
                //    {
                //        govWin.User_Name__c = data.ToString();
                //    }
                //}

                govWin.Client_Id__c = Convert.ToString(cases.Attributes["govwin_clientid"]);
                govWin.Client_Secret__c = Convert.ToString(cases.Attributes["govwin_clientsecret"]);
                govWin.End_Point__c = Convert.ToString(cases.Attributes["govwin_endpoint"]);
                govWin.Password__c = Convert.ToString(cases.Attributes["govwin_password"]);
                govWin.User_Name__c = Convert.ToString(cases.Attributes["govwin_username"]);


                var urldata = "https://services.govwin.com/neo-ws/oauth/token?" + "client_id=" + govWin.Client_Id__c + "&client_secret=" + govWin.Client_Secret__c + "&grant_type=password&username=" + govWin.User_Name__c + "&password=" + govWin.Password__c + "&scope=read";
                var request2 = (HttpWebRequest)HttpWebRequest.Create(urldata);
                request2.Headers.Add("x-ms-version", "2012-08-01");
                request2.Method = "POST";
                request2.ContentType = "application/json";
                request2.Method = "POST";
                request2.KeepAlive = false;
                Stream dataStream = request2.GetRequestStream();
                WebResponse response2 = request2.GetResponse();
                dataStream = response2.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                using (var streamReader = new StreamReader(response2.GetResponseStream()))
                {
                    tracer.Trace("get opportunity got response into stream reader");
                    var resultss = streamReader.ReadToEnd();

                    try
                    {
                        var json = JsonConvert.DeserializeObject<TokenClass>(resultss);
                        govWin.Access_Token__c = json.access_token;
                        govWin.Refresh_Token__c = json.refresh_token;
                    }
                    catch (WebException ex)
                    { }

                    tracer.Trace("get opportunity got response into stream reader opplist " + opplist.Count);
                }

                var url = "https://services.govwin.com/neo-ws/opportunities/OPP" + govWin.OppId;
                try
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    var request1 = (HttpWebRequest)WebRequest.Create(url);
                    //request1.Host = "iq.qa1.govwin.com";
                    request1.Headers["Authorization"] = "bearer" + govWin.Access_Token__c;
                    request1.Headers["Accept-Encoding"] = "gzip,deflate";
                    request1.Method = "GET";
                    request1.KeepAlive = false;
                    acessToken = "bearer" + govWin.Access_Token__c;
                    try
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        WebResponse response1 = request1.GetResponse();
                        using (var streamReader = new StreamReader(response1.GetResponseStream()))
                        {
                            var result = streamReader.ReadToEnd();
                            var json = JsonConvert.DeserializeObject<RootObject>(result);
                            opplist = json.opportunities;
                        }
                        response1.Dispose();
                    }
                    catch (Exception exe)
                    {
                        tracer.Trace("get opportunity error got response into stream reader");

                    }
                    response2.Dispose();
                }
                catch (Exception exe)
                {
                    tracer.Trace("get opportunity error got response into stream reader");
                }
            }

            catch (Exception ex)
            {
                tracer.Trace("get opportunity errorouter url");
            }
            return opplist;
        }

        public bool postopprtunity(Opportunity op, IOrganizationService service, ITracingService tracer, Entity Opportunities, IPluginExecutionContext context)
        {
            try
            {
                //try
                //{
                //tracer.Trace("Into test entity data add ");
                //    Guid testentityguid = new Guid();
                //    Entity testentity = new Entity("new_testentity");
                //    testentity["new_name"] = "test entity lookup on opportunity record plugin";
                //    testentityguid = service.Create(testentity);
                //    Opportunities["new_testentitylookup"] = new EntityReference(id: testentityguid, logicalName: "new_testentity");
                //    tracer.Trace("testentityguid:- " + testentityguid);
                //}
                //catch (Exception ex)
                //{
                //    tracer.Trace("Test Entity Error:-" + ex.Message.ToString());
                //}
               

                tracer.Trace("Into Opportunity data add ");
                //Contract
                Guid contactsguid = new Guid();
                tracer.Trace("contactsguid:-> " + contactsguid);
                // Entity Opportunities = new Entity("opportunity");
                Entity CHR = new Entity("govwin_govcontacts");
                if (op.links.contacts.href != "" && op.links.contacts.href != null)
                {
                    try
                    {
                        CHR["govwin_href"] = op.links.contacts.href;
                        CHR["govwin_name"] = "Contacts";//
                        CHR["govwin_token"] = acessToken.ToString();
                        contactsguid = service.Create(CHR);
                        Opportunities["govwin_govwincontacts"] = new EntityReference(id: contactsguid, logicalName: "govwin_govcontacts");
                        tracer.Trace("contactsguid:- " + contactsguid);
                    }
                    catch (Exception ex)
                    {
                        tracer.Trace("Contact Create Error:- " + ex.Message.ToString());
                    }


                }
                //Related Documents
                Guid relatedDocguid = new Guid();
                // Entity Opportunities = new Entity("opportunity");
                Entity RD = new Entity("govwin_govrelateddocuments");
                if (op.links.relatedDocuments.href != "" && op.links.relatedDocuments.href != null)
                {
                    RD["govwin_href"] = op.links.relatedDocuments.href;
                    RD["govwin_name"] = "Related Documents";//
                    RD["govwin_token"] = acessToken.ToString();
                    relatedDocguid = service.Create(RD);
                    Opportunities["govwin_govwinrelateddocuments"] = new EntityReference(id: relatedDocguid, logicalName: "govwin_govrelateddocuments");
                }

                //////////.....CONTRACT.........../////////////////////
                Guid contractsguid = new Guid();                             //3
                Entity RDHR = new Entity("govwin_govcontracts");
                if (op.links.contracts.href != null && op.links.contracts.href != "")
                {
                    RDHR["govwin_href"] = op.links.contracts.href;
                    RDHR["govwin_name"] = "Contracts";
                    RDHR["govwin_token"] = acessToken.ToString();
                    contractsguid = service.Create(RDHR);
                    Opportunities["govwin_govwincontracts"] = new EntityReference(id: contractsguid, logicalName: "govwin_govcontracts");
                }

                //////////.....Update.........../////////////////////
                //////////////////////////////////////////////////////
                Entity update = new Entity("govwin_govupdates");

                update["govwin_href"] = op.links.contracts.href;
                update["govwin_name"] = "Update";
                update["govwin_opportunitylookup"] = new EntityReference(id: Opportunities.Id, logicalName: "opportunity");
                service.Create(update);

                //Related Articles
                Guid relatedArticlesguid = new Guid();
                Entity RA = new Entity("govwin_govrelatedarticles");//updated on 16 feb

                if (op.links.relatedArticles.href != null && op.links.relatedArticles.href != "")
                {
                    RA["govwin_href"] = op.links.relatedArticles.href;
                    RA["govwin_name"] = "Article";
                    RA["govwin_token"] = acessToken.ToString();
                    relatedArticlesguid = service.Create(RA);
                    Opportunities["govwin_govwinrelatedarticles"] = new EntityReference(id: relatedArticlesguid, logicalName: "govwin_govrelatedarticles");
                }

                //////////.....Milestone.........../////////////////////
                //////////////////////////////////////////////////////
                Guid milestonesguid = new Guid();                                    //4
                Entity MilestoneEntity = new Entity("govwin_govmilestones");
                if (op.links.milestones.href != null && op.links.milestones.href != "")
                {
                    MilestoneEntity["govwin_href"] = op.links.milestones.href;
                    MilestoneEntity["govwin_name"] = "Milestone";
                    MilestoneEntity["govwin_token"] = acessToken.ToString();
                    milestonesguid = service.Create(MilestoneEntity);
                    Opportunities["govwin_govwinmilestones"] = new EntityReference(id: milestonesguid, logicalName: "govwin_govmilestones");
                }
                //////////.....Gov Entity.........../////////////////////
                //////////////////////////////////////////////////////

                Guid govEntityguid = new Guid();                              //9
                Entity GovEn = new Entity("govwin_govgov_entites");
                if (op.links.govEntity.href != "" && op.links.govEntity.href != null)
                {
                    GovEn["govwin_href"] = op.links.govEntity.href;
                    GovEn["govwin_name"] = "Entities";
                    GovEn["govwin_token"] = acessToken.ToString();
                    govEntityguid = service.Create(GovEn);
                    Opportunities["govwin_govwinentities"] = new EntityReference(id: govEntityguid, logicalName: "govwin_govgov_entites");
                }


                Guid fboNoticesguid = new Guid();
                Entity FN = new Entity("govwin_govfbo_notices");
                if (op.links.fboNotices.href != null && op.links.fboNotices.href != "")
                {
                    FN["govwin_href"] = op.links.fboNotices.href;
                    FN["govwin_name"] = "Notices";
                    FN["govwin_token"] = acessToken.ToString();
                    fboNoticesguid = service.Create(FN);

                    Opportunities["govwin_govwinfbonotices"] = new EntityReference(id: fboNoticesguid, logicalName: "govwin_govfbo_notices");
                }
                ////////////////////Companies
                Guid companiesguid = new Guid();
                Entity CO = new Entity("govwin_govcompanies");
                if (op.links.companies.href != null && op.links.companies.href != "")
                {
                    CO["govwin_href"] = op.links.companies.href;
                    CO["govwin_name"] = "Companies";
                    CO["govwin_token"] = acessToken.ToString();
                    companiesguid = service.Create(CO);

                    Opportunities["govwin_govwincompanies"] = new EntityReference(id: companiesguid, logicalName: "govwin_govcompanies");
                }

                ////////////////////Place Of Performances
                Guid POPGuid = new Guid();
                Entity POP = new Entity("govwin_govplaceofperformance");
                if (op.links.placesOfPerformance.href != null && op.links.placesOfPerformance.href != "")
                {
                    POP["govwin_href"] = op.links.placesOfPerformance.href;
                    POP["govwin_name"] = "Place Of Performances";
                    POP["govwin_token"] = acessToken.ToString();
                    POPGuid = service.Create(POP);
                    Opportunities["govwin_govwinplaceofperformances"] = new EntityReference(id: POPGuid, logicalName: "govwin_govplaceofperformance");
                }
                ////////////Contract Vehicles
                Guid CVGuid = new Guid();
                Entity CV = new Entity("govwin_govcontractvehicles");
                if (op.links.contractVehicles.href != null && op.links.contractVehicles.href != "")
                {
                    try
                    {
                        CV["govwin_href"] = op.links.contractVehicles.href;
                        CV["govwin_name"] = "Contract Vehicles";
                        CV["govwin_token"] = acessToken.ToString();
                        CVGuid = service.Create(CV);
                        Thread.Sleep(10000);
                        Opportunities["govwin_govwincontractvehicles"] = new EntityReference(id: CVGuid, logicalName: "govwin_govcontractvehicles");
                    }
                    catch (Exception e)
                    {
                    }

                }

                EntityReferenceCollection competiontypes = new EntityReferenceCollection();

                EntityReferenceCollection contrctype = new EntityReferenceCollection();
                foreach (var comptyp in op.competitionTypes)
                {
                    Guid comptypeguid = new Guid();
                    Entity comptype = new Entity("govwin_govwincompetitiontype");
                    comptype["govwin_name"] = "GovWinCompetionType";
                    comptype["govwin_id"] = Convert.ToString(comptyp.id);
                    comptype["govwin_title"] = comptyp.title;
                    comptypeguid = service.Create(comptype);

                    EntityReference CT = new EntityReference();
                    CT.LogicalName = "govwin_govwincompetitiontype";
                    CT.Id = comptypeguid;
                    competiontypes.Add(CT);
                }
                tracer.Trace("Into Opportunity govwin_govwincompetitiontype relationship ");
                foreach (var contractTypes in op.contractTypes)
                {
                    Guid ctguid = new Guid();
                    Entity CT = new Entity("govwin_govcontracttypes");
                    CT["govwin_govwincontractid"] = Convert.ToString(contractTypes.id);
                    CT["govwin_name"] = "GovWinContractType";
                    CT["govwin_govwincontracttitle"] = contractTypes.title;
                    ctguid = service.Create(CT);
                    EntityReference CNT = new EntityReference();
                    CNT.Id = ctguid;
                    CNT.LogicalName = "govwin_govcontracttypes";
                    contrctype.Add(CNT);

                }

                if (op.awardDate != null)
                {
                    if (op.awardDate.deltekEstimate != "" && op.awardDate.deltekEstimate != null)
                    {
                        Opportunities["govwin_awarddatedeltekestimate"] = Convert.ToBoolean(op.awardDate.deltekEstimate);
                    }
                    if (op.awardDate.value != "" && op.awardDate.value != null)
                    {
                        string Formatdate = op.awardDate.value.Replace("T", " ");
                        Opportunities["govwin_award_date_value"] = Formatdate;
                    }
                    if (op.awardDate.govtEstimate != "" && op.awardDate.govtEstimate != null)
                    {
                        Opportunities["govwin_awarddategovtestimate"] = Convert.ToBoolean(op.awardDate.govtEstimate);
                    }
                }



                if (op.primaryNAICS != null)
                {
                    if (op.primaryNAICS.id != "" && op.primaryNAICS.id != null)
                    {
                        Opportunities["govwin_primary_naics_id"] = op.primaryNAICS.id;
                    }
                    if (op.primaryNAICS.sizeStandard != "" && op.primaryNAICS.sizeStandard != null)
                    {
                        Opportunities["govwin_primary_naics_size_standard"] = op.primaryNAICS.sizeStandard;
                    }

                    if (op.primaryNAICS.title != "" && op.primaryNAICS.title != null)
                    {
                        Opportunities["govwin_primary_naics_title"] = op.primaryNAICS.title;
                    }
                }


                if (op.description != "" && op.description != null)
                {
                    Opportunities["govwin_description"] = op.description;
                }


                if (op.duration != "" && op.duration != null)
                {
                    Opportunities["govwin_duration"] = op.duration;
                }



                if (op.govEntity.id != null)
                {
                    Opportunities["govwin_gov_entity_id"] = Convert.ToString(op.govEntity.id);
                }



                if (op.govEntity.title != "" && op.govEntity.title != null)
                {
                    Opportunities["govwin_gov_entity_title"] = op.govEntity.title;
                }


                if (op.id != "" && op.id != null)
                {
                    //Opportunities["govwin_govwin_opportunity_id"] = op.id;
                    Opportunities["govwin_govwin_opportunity_id"] = Convert.ToString(op.iqOppId);
                }


                if (op.iqOppId != null)
                {
                    Opportunities["govwin_iqopportunity_id"] = Convert.ToString(op.iqOppId);
                }
                if (op.oppValue != null)
                {
                    Opportunities["govwin_opportunity_value"] = Convert.ToString(op.oppValue);
                }
                if (op.primaryRequirement != "" && op.primaryRequirement != null)
                {
                    Opportunities["govwin_primary_requirement"] = op.primaryRequirement;
                }


                if (op.procurement != "" && op.procurement != null)
                {
                    Opportunities["govwin_procurement"] = op.procurement;
                }
                if (op.solicitationNumber != "" && op.solicitationNumber != null)
                {
                    Opportunities["govwin_solicitation_number"] = op.solicitationNumber;
                }
                if (op.solicitationDate != null)
                {
                    if (op.solicitationDate.value != "" && op.solicitationDate.value != null)
                    {
                        string Formatdate = op.solicitationDate.value.Replace("T", " ");
                        Opportunities["govwin_solicitation_date_value"] = Formatdate;
                    }
                    if (op.solicitationDate.govtEstimate != "" && op.solicitationDate.govtEstimate != null)
                    {
                        Opportunities["govwin_solicitationdateestimate"] = Convert.ToBoolean(op.solicitationDate.govtEstimate);
                    }
                    if (op.solicitationDate.deltekEstimate != "" && op.solicitationDate.deltekEstimate != null)
                    {
                        Opportunities["govwin_solicitationdatedeltekestimate"] = Convert.ToBoolean(op.solicitationDate.deltekEstimate);
                    }
                }



                if (op.sourceURL != "" && op.sourceURL != null)
                {
                    Opportunities["govwin_source_url"] = op.sourceURL;
                }


                if (op.status != "" && op.status != null)
                {
                    Opportunities["govwin_status"] = op.status;
                }
                if (op.title != "" && op.title != null)
                {
                    Opportunities["govwin_title"] = op.title;
                }
                if (op.typeOfAward != "" && op.typeOfAward != null)
                {
                    Opportunities["govwin_type_of_award"] = op.typeOfAward;
                }

                if (op.updateDate != "" && op.updateDate != null)
                {
                    string Formatdate = op.updateDate.Replace("T", " ");
                    Opportunities["govwin_update_date"] = Formatdate;
                }
                if (op.type != "" && op.type != null)
                {
                    Opportunities["govwin_type"] = op.type;
                }
                if (op.priority > 0 && op.priority < 6)
                {
                    Opportunities["govwin_priority"] = Convert.ToInt32(op.priority);
                }
                if (op.internalStatus != "" && op.internalStatus != null)
                {
                    Opportunities["govwin_internalstatus"] = op.internalStatus;
                }


                // Opportunities["govwin_govwinrelateddocuments"] = new EntityReference(id: relatedDocumentsguid, logicalName: "govwin_govrelateddocuments");
                //Opportunities["name"] = "GovWinOpportunity";
                context.InputParameters["Target"] = Opportunities;
                service.Update(Opportunities);
                //EntityReference er = new EntityReference("opportunity", OldOpportunities.Id);

                //Opportunities["govwin_govinopportunity"] = er;
                //Opportunities.Id = cguid;
                //service.Update(Opportunities);
                //tracer.Trace("Into Opportunity Updated Opportunity  ");
                if (competiontypes.Count > 0)
                {
                    tracer.Trace("Into Opportunity Updated Opportunity  competiontypes count " + competiontypes.Count);
                    Relationship relationship = new Relationship("govwin_opportunity_govwin_govwincompetitiontype");
                    service.Associate("opportunity", Opportunities.Id, relationship, competiontypes);

                    tracer.Trace("Into Opportunity Updated Opportunity  competiontypes");

                }
                if (contrctype.Count > 0)
                {
                    Relationship relationship1 = new Relationship("govwin_opportunity_govwin_govcontracttypes");
                    service.Associate("opportunity", Opportunities.Id, relationship1, contrctype);

                }

            }
            catch (Exception ex)
            {
                tracer.Trace("Into Opportunity Updated Opportunity  error " + ex.Message);
            }
            return true;
        }

    }
    public class Links
    {
    }
    public class RootObject
    {
        public Links links { get; set; }
        public List<Opportunity> opportunities { get; set; }
    }

    public class PrimaryNAICS
    {
        public string id { get; set; }
        public string title { get; set; }
        public string sizeStandard { get; set; }
    }

    public class AwardDate
    {
        public string deltekEstimate { get; set; }
        public string govtEstimate { get; set; }
        public string value { get; set; }
    }

    public class CompetitionType
    {
        public int id { get; set; }
        public string title { get; set; }
    }

    public class ContractType
    {
        public int id { get; set; }
        public string title { get; set; }
    }

    public class GovEntity2
    {
        public int id { get; set; }
        public string title { get; set; }
    }

    public class SolicitationDate
    {
        public string deltekEstimate { get; set; }
        public string govtEstimate { get; set; }
        public string value { get; set; }
    }

    public class GovEntity
    {
        public string href { get; set; }
    }
    public class WebHref
    {
        public string href { get; set; }
    }

    public class Contacts
    {
        public string href { get; set; }
    }

    public class RelatedDocuments
    {
        public string href { get; set; }
    }

    public class FboNotices
    {
        public string href { get; set; }
    }

    public class Contracts
    {
        public string href { get; set; }
    }

    public class Milestones
    {
        public string href { get; set; }
    }

    public class Companies
    {
        public string href { get; set; }
    }

    public class RelatedArticles
    {
        public string href { get; set; }
    }

    public class PlacesOfPerformance
    {
        public string href { get; set; }
    }

    public class ContractVehicles
    {
        public string href { get; set; }
    }

    [DataContract]
    public class TokenClass
    {
        [DataMember]
        public string access_token { get; set; }
        [DataMember]
        public string token_type { get; set; }
        [DataMember]
        public string refresh_token { get; set; }
        [DataMember]
        public int expires_in { get; set; }
        [DataMember]
        public string scope { get; set; }
    }
}

