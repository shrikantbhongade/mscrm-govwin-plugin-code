﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Gov_Win_Plugin
{
    public class RelatedDocument : IPlugin
    {

        public string Authtoken = "";
        public static string acessToken;
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            if (context.Depth == 2)
            {
                return;
            }
            try
            {
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    tracer.Trace("Into GovWin plugin to call api Related Documents");
                    //var targetEntity = (Entity)context.InputParameters["Target"];
                    //if (targetEntity.LogicalName != "govwin_govrelateddocuments")
                    //    return;
                    ////Entity RelatedArticles = service.Retrieve("govwin_govrelateddocuments", targetEntity.Id, new ColumnSet(true));
                    //Entity RelatedArticles = (Entity)context.InputParameters["Target"];
                    var opportunity = (Entity)context.InputParameters["Target"];
                    var targetEntity = opportunity.GetAttributeValue<EntityReference>("govwin_govwinrelateddocuments");
                    Entity RelatedArticles = service.Retrieve("govwin_govrelateddocuments", targetEntity.Id, new ColumnSet(new string[] { "govwin_token", "govwin_href" }));
                    if (RelatedArticles.Attributes.Contains("govwin_token") && RelatedArticles.Attributes.Contains("govwin_href"))
                    {
                        var ovrelateddoclist = getrelatedArticles(RelatedArticles.Attributes["govwin_href"].ToString(), RelatedArticles.Attributes["govwin_token"].ToString(), tracer);
                        EntityReferenceCollection ovrelateddocEntity = new EntityReferenceCollection();
                        Guid RAguid = new Guid();
                        int i = 0;
                        tracer.Trace("Into  Related Documents after response " + ovrelateddoclist.Count());
                        foreach (var c in ovrelateddoclist)
                        {
                            i++;
                            try
                            {



                                Entity RAE = new Entity("govwin_subgovrelateddocument");
                                if (c.documentDownloadURL != "" && c.documentDownloadURL != null)
                                {
                                    RAE["govwin_documentdownloadurl"] = (c.documentDownloadURL);
                                }
                                if (c.documentURL != "" && c.documentURL != null)
                                {
                                    RAE["govwin_documenturl"] = (c.documentURL);
                                }
                                if (c.id != 0)
                                {
                                    RAE["govwin_id"] = c.id;
                                    RAE["govwin_id2"] = Convert.ToString(c.id);
                                }
                                if (c.publicationDate != null)
                                {
                                    RAE["govwin_publicationdate"] = Convert.ToDateTime(c.publicationDate).ToUniversalTime();
                                }
                                if (c.sourceDescription != "" && c.sourceDescription != null)
                                {
                                    RAE["govwin_sourcedescription"] = (c.sourceDescription);
                                }
                                if (c.title != null && c.title != "")
                                {
                                    RAE["govwin_title"] = (c.title);
                                }
                                if (c.fileName != null && c.fileName != "")
                                {
                                    RAE["govwin_filename"] = (c.fileName);
                                }
                                if (c.fileTypeDescription != null && c.fileTypeDescription != "")
                                {
                                    RAE["govwin_filetypedescription"] = (c.fileTypeDescription);
                                }
                                RAE["govwin_name"] = (i.ToString());
                                EntityReference erf = new EntityReference("govwin_govrelateddocuments", targetEntity.Id);
                                RAE["govwin_relateddocuments"] = erf;

                                RAguid = service.Create(RAE);
                                EntityReference CnTr = new EntityReference();
                                CnTr.LogicalName = "govwin_subgovrelateddocument";
                                CnTr.Id = RAguid;
                                ovrelateddocEntity.Add(CnTr);
                                //c.documentURL
                                if (c.documentDownloadURL.ToString() != "")
                                {
                                    //Thread.Sleep(3000);
                                    byte[] bt = getdocuments(c.documentDownloadURL.ToString(), RelatedArticles.Attributes["govwin_token"].ToString(), tracer, c.fileName);

                                    String filedata = Convert.ToBase64String(bt);
                                    Entity annotation = new Entity("annotation");
                                    annotation["filename"] = c.fileName.ToString();
                                    annotation["subject"] = "Attachment";
                                    annotation["documentbody"] = filedata;
                                    annotation["objectid"] = new EntityReference("govwin_subgovrelateddocument", RAguid);
                                    //  annotation["mimetype"] = "text/plain";
                                    var cr = service.Create(annotation);
                                }

                            }
                            catch (Exception ex)
                            {
                                tracer.Trace("Into  Related Documents after response added erreor ");
                            }
                        }

                    }

                }
                //TODO: Do stuff
            }
            catch (Exception e)
            {
                //throw new InvalidPluginExecutionException(e.Message);
            }
        }
        public byte[] getdocuments(string url, string token, ITracingService tracer, string name)
        {
            try
            {

                //string durl = "https://demo.parature.com/FileManagement/Download/19c241c6d2f941b88cfba9c7594d906a";
                string durl = url;
                string dtoken = token;
                //tracer.Trace("Into  Related Documents download url " + durl);
                //tracer.Trace("Into  Related Documents download token " + dtoken);
                try
                {

                    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    var request = (HttpWebRequest)WebRequest.Create(durl);
                    //String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                    request.KeepAlive = false; // App Source
                    tracer.Trace("govind token" + dtoken);
                    tracer.Trace("govind url" + url);
                    request.Headers.Add("Accept-Encoding", "gzip,deflate");
                    request.Host = "iq.qa1.govwin.com";
                    // request.Headers.Add("Host", "iq.qa1.govwin.com");
                    request.Headers["Authorization"] = dtoken;
                    request.Method = "GET";
                    //request.ContentType = "multipart/form-data";
                    MemoryStream ms = new MemoryStream();
                    //Stream s;
                    //byte[] RDData;

                    WebResponse response = request.GetResponse();
                    try
                    {
                        tracer.Trace("Into  Related Documents download response ");
                        using (Stream input = response.GetResponseStream())
                        {
                            input.CopyTo(ms);
                            return ms.ToArray();
                        }
                    }
                    catch (Exception ex)
                    {
                        response.Dispose();
                    }
                    response.Dispose();
                }
                catch (Exception ex)
                {
                    //tracer.Trace("error in request " + ex.Message + " " + ex.ToString());
                    //if (ex.InnerException != null)
                    //    tracer.Trace("error in request " + ex.Message + " " + ex.InnerException.ToString());
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<DocumentLinksRD> getrelatedArticles(string url, string token, ITracingService trace)
        {

            List<DocumentLinksRD> DocumentLinkslist = new List<DocumentLinksRD>();
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Headers["Authorization"] = token;
                request.Method = "GET";
                request.KeepAlive = false; // App Source
                WebResponse response = request.GetResponse();
                try
                {

                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {

                        var result = streamReader.ReadToEnd();
                        var k = result;
                        var json = JsonConvert.DeserializeObject<RootObjectRDoc>(result);

                        DocumentLinkslist = json.relatedDocuments;
                    }
                }
                catch (Exception exe)
                {
                    response.Dispose(); // App Source
                }
                response.Dispose(); // App Source
            }
            catch (Exception exe)
            {
            }

            return DocumentLinkslist;
        }


    }

    //Related Documents Model
    #region

    public class DocumentLinksRD
    {
        public string documentDownloadURL { get; set; }
        public string documentURL { get; set; }
        public string fileName { get; set; }
        public string fileTypeDescription { get; set; }
        public int id { get; set; }
        public DateTime publicationDate { get; set; }
        public string title { get; set; }
        public string sourceDescription { get; set; }

    }

    public class RootObjectRDoc
    {
        public Links links { get; set; }
        public Meta meta { get; set; }
        public List<DocumentLinksRD> relatedDocuments { get; set; }
    }
    #endregion
}
